var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');


//Importa las rutas
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users.route');
var postsRouter = require('./routes/posts.route');
var messageRouter = require('./routes/messages.route');



//Importa la dbmanager
const dbmanager = require('./database/db.manager');


var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//IMPORTANTE 
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/posts', postsRouter);
app.use('/message', messageRouter);
// se prueba la Conexion y si es exitosa se sincroniza y si no bota el error
dbmanager.sequelizeConexion.authenticate().then(
    () => {
        console.log("**************Se ha conectado a su base de datos mysqlWorkbench*******************");
        dbmanager.sequelizeConexion.sync().then(
            ()=>{
                console.log("Base de datos Sincronizada");
            }

        );
    }

).catch(
    error => {
        console.log("Error de conexion en la base de datos",error);
    }

)

module.exports = app;
