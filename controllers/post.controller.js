

const dbmanager = require('../database/db.manager');
;

/**
 * 
 * @param {*} req 
 * @param {*} res 
 */

 async function crearpost(req,res){
    // verificar que el req no este vacio
    if(!req.body){
        res.status(400).send(
            {

            message: "Request esta vacio"

             }
        );
        return;
        
    }
   
    // crear el objeto usuario
    

   
    const newPostObject ={    
        
        idPost: req.body.idPost,
        username: req.body.username,
        message: req.body.message
    }
    // Insertando en la abse de datos el objeto
    dbmanager.Post.create(newPostObject).then(
        data => {
            res.send(data);
        }

    ).catch(
        error => {
            console.log(error);
            res.status (500).send(
                {
                    message: " ERRRRRROOOOOOOOORRRRRRRR"+error
                }

            );
        }    
        
        
    );

}


async function allposts(req,res){
    try{const allposts = await dbmanager.Post.findAll();

        res.send(
            {
                data: allposts
            }
        );
    }catch(error){
        console.log(error)
        res.status(500).send(
            {
                message:"ERRORR,LO SENTIMOS ESTAMOS TRABAJANDO PARA SOLUCIONARLO"
            }
        )
    }
}

async function findPostByUser(req,res){
    try{

        // se trae el parametro
       const { username } = req.params;

       const us = await dbmanager.Post.findAll(
           {
               where: {
                   username: username
               }
           }
       );

        res.json(us);

    }catch(esrror){
        console.log(error,"Perdonameeeeee")
        res.status(500).send(
            {
                message:"ERRORR,LO SENTIMOS ESTAMOS TRABAJANDO PARA SOLUCIONARLO,"
            }
        )

    }
}



async function DeleteById (req,res) {
    const { idPost} = req.params;
    
   await dbmanager.Post.destroy({
        where: {
            idPost : idPost
        }
     })
     res.send(
         {
             message: "Eliminado"
         }
     )
    }
	
	
exports.crearpost = crearpost;
exports.allposts = allposts;
exports.findPostByUser = findPostByUser;
exports.DeleteById = DeleteById;
