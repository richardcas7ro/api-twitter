var express = require('express');
var router = express.Router();
const messageController = require ('../controllers/message.controller'); 
/**
 * GET Route to create a message
 */
router.post('/createMessage', messageController.createMessage);
/**
 * GET Route to list of messages
 */
router.get('/selectMessage/:Sender,:Receiver', messageController.selectMessage);




/**
 * TASK:
 * ADD THE MISSING ROUTES ______________________________________________________ 
 */

// Export router
module.exports = router;

