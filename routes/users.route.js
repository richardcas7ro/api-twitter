var express = require('express');
var router = express.Router();
const Usercontroller = require('../controllers/user.controller');


/* Post users listing. */
router.post('/',Usercontroller.crearusuario );
// Se puede dejar la misma ruta pues  son diferentes metodos
// Get todos los usuarios
router.get('/',Usercontroller.findallusers );

//Get usuario por id
router.get('/:idUser',Usercontroller.findUserById );

router.delete('/:idUser',Usercontroller.DeleteByIdusuario );
/**
 * Get Route to validate a username
 */
router.get('/ValidateOneUser/:username',userController.ValidateOneUser);

module.exports = router;
