const Sequelize = require('sequelize');

const sequelizeConexion = require('./db.conexion');


// Importar modelos

const UserModel = require('../models/user.model');
const PostModel = require('../models/post.model');
const MessageModel = require("../model/message.model");


//Inicializar modelos

const User = UserModel(sequelizeConexion,Sequelize);
const Post = PostModel(sequelizeConexion,Sequelize);
const Message = MessageModel (sequelizeConnection, Sequelize);

User.hasMany(Post,{ foreingkey:'idPost',sourcekey:'idUser'});
Post.belongsTo(User,{foreingkey:'idUser',sourcekey:'idPost'});

User.hasMany(Message, { foreignKey: 'idMessage', sourceKey: 'idUser' });
Message.belongsTo( User, { foreignKey: 'idUser', sourceKey: 'idMessage' });
Message.belongsTo( User, { foreignKey: 'idSender', sourceKey: 'idMessage' });
//Message.hasMany(User, { foreignKey: 'idUser', sourceKey: 'idSender' });


const models = {
    User: User,
    Post: Post,
	Message: Message,
}
const db ={

    ...models,
    sequelizeConexion
}

module.exports= db;
