module.exports = (sequelize, Sequelize) =>{
    const Message = sequelize.define ("Message", {
        idMessage: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        message: Sequelize.STRING,
        published_date: Sequelize.DATE,
        idSender: Sequelize.INTEGER,
    }, {
        tableName: "messges"
    });
    return Message;
}
