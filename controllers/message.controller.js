const dbManager = require ('../database.config/database.manager');

/**
 * Creation of a message sender reciver
 * @param {*} messageObject JSON Object with Message information
 */
async function createMessage (req, res) {
    
    // CHECK IF THE REQUEST BODY IS EMPTY
    if (!req.body) {
        res.status(400).send({
          message: "Request body is empty!!!!"
        });
        return;
    }
    
    //GET USERS IDS
    const { idUser1 } = req.body.sender;
    const { idUser2 } = req.body.receiver;
    const { bod }  = req.body.sender;

    //Execute query
    const user1 = await dbManager.User.findOne({
        where: {
            username: req.body.sender
        }
    });
    
    const user2 = await dbManager.User.findOne({
        where: {
            username: req.body.receiver
        }
    });
    //check response
    if ( user1 == null ){
        res.status(403).send({
            message: "Requested user " + req.body.Sender + " don´t exist"
          });
          return;
    }
    if ( user2 == null ){
        res.status(403).send({
            message: "Requested user " + req.body.Receiver + " don´t exist"
          });
          return;
    }

    // CREATING THE OBJECT TO PERSIST
    const newMessageObject = {
        message: req.body.message,
        published_date: req.body.published_date,
        idSender: user1.idUser,
        idUser: user2.idUser
    }
    
    // EXECUTING THE CREATE QUERY - INSERT THE OBJECT INTO DATABASE 
    dbManager.Message.create(newMessageObject).then (
        data => {
            res.send (data);
        }
    ).catch (
        e => {
            // Print error on console
            console.log(e);
            // Send error message as a response 
            res.status(500).send({
                message: "Some error occurred"
            });
        }
    );
}


/**
 * Creation of a message sender reciver
 * @param {*} messageObject JSON Object with Message information
 */
async function selectMessage (req, res) {
    
    // CHECK IF THE REQUEST BODY IS EMPTY
    if (!req.body) {
        res.status(400).send({
          message: "Request body is empty!!!!"
        });
        return;
    }
    
    //GET USERS IDS
    const { idUser1 } = req.params.Sender;
    const { idUser2 } = req.params.Receiver;

    //Execute query
    //Execute query
    try{
   
        const user1 = await dbManager.User.findOne({
            where: {
                username: req.params.Sender
            }
        });
        
        const user2 = await dbManager.User.findOne({
            where: {
                username: req.params.Receiver
            }
        });
    
        //check response
        if ( user1 == null ){
            res.status(403).send({
                message: "Requested user " + req.params.Sender + " don´t exist"
            });
            return;
        }
        if ( user2 == null ){
            res.status(403).send({
                message: "Requested user " + req.params.Receiver + " don´t exist"
            });
            return;
        }

        // EXECUTING THE CREATE QUERY - INSERT THE OBJECT INTO DATABASE 
        const messages = await dbManager.Message.findAll({
            where: {
                idSender: user1.dataValues.idUser,
                idUser: user2.dataValues.idUser
            }
        });

        res.json(messages);

    }catch (e) {
        // Print error on console
        console.log(e);
        // Send error message as a response 
        res.status(500).send({
            message: "Some error occurred"
        });
    }

   


}


exports.createMessage = createMessage; 
exports.selectMessage = selectMessage;