// por cada modelo se hace un controlador

const dbmanager = require('../database/db.manager');

/**
 * 
 * @param {*} req 
 * @param {*} res 
 */

function crearusuario(req,res){
    // verificar que el req no este vacio
    if(!req.body){
        res.status(400).send(
            {
            message: "Request esta vacio"
             }
        );
        return;
        
    }
    // crear el objeto usuario
    const newUserObject ={
        username: req.body.username,
        creationdate: req.body.creationdate
    }
    // Insertando en la abse de datos el objeto
    dbmanager.User.create(newUserObject).then(
        data => {
            res.send(data);
        }

    ).catch(
        error => {
            console.log(error);
            res.status (500).send(
                {
                    message: " ERRRRRROOOOOOOOOORRRRRRRR"

                }

            );
        }    
        
        
    );

}

async function findallusers(req,res){
    try{const allusers = await dbmanager.User.findAll();

        res.send(
            {
                data: allusers
            }
        );
    }catch(error){
        console.log(error)
        res.status(500).send(
            {
                message:"ERRORR,LO SENTIMOS ESTAMOS TRABAJANDO PARA SOLUCIONARLO"
            }
        )
    }
}

async function findUserById(req,res){
    try{

        // se trae el parametro
       const { idUser } = req.params;

       const user = await dbmanager.User.findOne(
           {
               where: {
                   idUser: idUser
               }
           }
       );

        res.json(user);

    }catch(esrror){
        console.log(error,"Perdonameeeeee")
        res.status(500).send(
            {
                message:"ERRORR,LO SENTIMOS ESTAMOS TRABAJANDO PARA SOLUCIONARLO,"
            }
        )
    }
}

async function DeleteByIdusuario (req,res) {
    const { idUser} = req.params;
   await dbmanager.User.destroy({
        where: {
            idUser : idUser
        }
     })
     res.send(
         {
             message: "Eliminado"
         }
     )
    }
	
	

/**
 * Get user by id
 */
async function ValidateOneUser (req, res){
    try {
        const { username } = req.params;

        //Execute query
        const user = await dbManager.User.findOne({
            where: {
                username: username
            }
        });
        //Send response
        if ( user == null ){
            res.json(false);
        }else{
            res.json(true);
        }         
    } catch (e) {
        // Print error on console
        console.log(e);
        // Send error message as a response 
        res.status(500).send({
            message: "Some error occurred"
        });
    }
    
}

exports.crearusuario = crearusuario;
exports.findallusers = findallusers;
exports.findUserById = findUserById;
exports.DeleteByIdusuario = DeleteByIdusuario;
exports.ValidateOneUser = ValidateOneUser;