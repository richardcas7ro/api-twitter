var express = require('express');
var router = express.Router();
const Postcontroller = require('../controllers/post.controller');

/* Post  posts  listing. */
router.post('/',Postcontroller.crearpost );
router.get('/',Postcontroller.allposts)
router.get('/:username',Postcontroller.findPostByUser);
router.delete('/:idPost',Postcontroller.DeleteById);



module.exports = router;
